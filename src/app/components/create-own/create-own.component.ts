import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Flowers } from 'src/app/models/flower';
import { Bouquets } from 'src/app/models/bouquet';
import { FlowerService } from 'src/app/services/flower.service';

import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as _moment from 'moment';
// Нужно исправить, но пока не знаю как именно, компилится, но красным подчеркивает
import { default as _rollupMoment } from 'moment';
import { element } from 'protractor';
import { BouquetService } from 'src/app/services/bouquet.service';
const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-create-own',
  templateUrl: './create-own.component.html',
  styleUrls: ['./create-own.component.scss'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'ru-RU' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})

export class CreateOwnComponent implements OnInit {
  flowers = new FormControl();
  colors = new FormControl();
  date = new FormControl(moment());

  flowersArr = [];
  deliveryFlowersArray = [];
  bouquetsArr = [];
  deliveryBouquetsArray = [];

  constructor(private flowerService: FlowerService, private bouquetService: BouquetService) { }

  ngOnInit() {
    this.flowersArr = this.flowerService.newFlowArr;
    this.deliveryFlowersArray = this.flowerService.flowerDeliveryArray;
    this.bouquetsArr = this.bouquetService.newBouqArr;
    this.deliveryBouquetsArray = this.bouquetService.bouquetDeliveryArray;
  }

  @Input() flower: Flowers;
  @Input() bouquet: Bouquets;

  // =======================From backend========================
  flowerList: string[] = ['All flowers', 'Rose', 'Lilies', 'Orchids'];

  // =======================From backend========================
  colorList: string[] = ['All colors', 'Red', 'Pink', 'White'];

}
