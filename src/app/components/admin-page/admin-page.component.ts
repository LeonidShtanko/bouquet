import { Component, Input, OnInit } from '@angular/core';
import { Flowers } from 'src/app/models/flower';
import { FlowerService } from 'src/app/services/flower.service';
import { Bouquets } from 'src/app/models/bouquet';
import { BouquetService } from 'src/app/services/bouquet.service';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.scss']
})
export class AdminPageComponent implements OnInit{
  originalFlowersArray = [];
  originalBouquetsArray = [];

  @Input() flower: Flowers;
  @Input() bouquet: Bouquets;

  constructor(private flowerService: FlowerService,private bouquetService: BouquetService) { }

  ngOnInit() {
    this.originalFlowersArray = this.flowerService.flowerCardsArray;
    this.originalBouquetsArray = this.bouquetService.bouquetCardsArray;
  }
}
