import { Component, OnInit, Input } from '@angular/core';
import { Bouquets } from 'src/app/models/bouquet';
import { BouquetService } from 'src/app/services/bouquet.service';

@Component({
  selector: 'app-bouquet',
  templateUrl: './bouquet.component.html',
  styleUrls: ['./bouquet.component.scss']
})
export class BouquetComponent implements OnInit {

  @Input() bouquet: Bouquets;

  bouquets = [];
  newArrBouquets = [];

  constructor(private service: BouquetService) { }

  ngOnInit() {
    this.bouquets = this.service.bouquetDeliveryArray;
    this.newArrBouquets = this.service.newBouqArr;
  }
  toggleCheck() {
    if (this.bouquet.isChecked === false) {
      this.bouquet.isChecked = true;
      for (let i = this.newArrBouquets.length - 1; i >= 0; i--) {
        if (this.newArrBouquets[i].isChecked === false) {
          this.newArrBouquets[i].isDisabled = true;
        }
      }
    } else {
      this.bouquet.isChecked = false;
      for (let i = this.newArrBouquets.length - 1; i >= 0; i--) {
        if (this.newArrBouquets[i].isChecked === false) {
          this.newArrBouquets[i].isDisabled = false;
        }
      }
    }
    this.bouquet.isChecked === true ? this.bouquets.push(this.bouquet) : this.bouquets.splice(0);
  };
}
