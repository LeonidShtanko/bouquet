import { Component, Input, OnInit } from '@angular/core';
import { Flowers } from './../../models/flower';
import { FlowerService } from 'src/app/services/flower.service';

@Component({
  selector: 'app-flower',
  templateUrl: './flower.component.html',
  styleUrls: ['./flower.component.scss']
})
export class FlowerComponent implements OnInit {

  countFlowers = [];

  constructor(private service: FlowerService) { }

  ngOnInit() {
    this.countFlowers = this.service.flowerDeliveryArray;
  }

  @Input() flower: Flowers;

  increaseFlowerCount() {
    this.flower.count++;
  }
  decreaseFlowerCount() {
    if (this.flower.count > 0) {
      this.flower.count--;
    }
  }
  // for check if name exists in the array
  nameExists(name) {
    return this.countFlowers.some((elem) => {
      return elem.name === name;
    })
  }
  // ============ Submit on each change in input =================
  submit() {
    if (this.flower.count > 1) { }
    if (this.flower.count == 1) {
      if (this.nameExists(this.flower.name)) { 
      } else {
        this.countFlowers.push(this.flower);
      }
    }
    if (this.flower.count == 0) {
      for(let i = this.countFlowers.length - 1; i>=0; --i) {
        if(this.countFlowers[i].name == this.flower.name){
          this.countFlowers.splice(i,1);
        }
      }
    }
  }
}
