import { Component, ComponentFactoryResolver, ViewChild } from '@angular/core';
import { ModalComponent } from '../modals/modal/modal.component';
import { RefDirective } from 'src/app/ref.directive';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  @ViewChild(RefDirective, { static: false }) refDir: RefDirective

  constructor(private resolver: ComponentFactoryResolver) { }

  showModal() {
    const modalFactory = this.resolver.resolveComponentFactory(ModalComponent);
    this.refDir.containerRef.clear()

    const modalLoginComponent = this.refDir.containerRef.createComponent(modalFactory);

    modalLoginComponent.instance.title = 'login'
    modalLoginComponent.instance.close.subscribe(() => {
      this.refDir.containerRef.clear()
    })
  }
}
