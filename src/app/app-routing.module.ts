import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CreateOwnComponent } from './components/create-own/create-own.component';
import { ClientPageComponent } from './components/client-page/client-page.component';
import { AdminPageComponent } from './components/admin-page/admin-page.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';

const routes: Routes = [
    {path: '', component: CreateOwnComponent},
    {path: 'client', component: ClientPageComponent},
    {path: 'admin', component: AdminPageComponent},
    {path: 'signup', component: SignUpComponent},
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule {

}