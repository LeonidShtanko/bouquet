import { NgModule } from '@angular/core';
import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatInputModule,
  MatSelectModule,
  MatButtonModule,
  MatCheckboxModule,
  MatBadgeModule,
  MatIconModule,
  MatCardModule,
  MatStepperModule,
  MatDividerModule,
  MatListModule,
  MatRadioModule,
  MatGridListModule,
} from '@angular/material';
import { DragDropModule } from '@angular/cdk/drag-drop';

const MaterialComponents = [
  MatDatepickerModule,
  MatInputModule,
  MatNativeDateModule,
  MatSelectModule,
  MatButtonModule,
  MatCheckboxModule,
  MatBadgeModule,
  MatIconModule,
  MatCardModule,
  MatStepperModule,
  MatDividerModule,
  MatListModule,
  MatRadioModule,
  MatGridListModule,
  DragDropModule,
]

@NgModule({
  imports: [MaterialComponents],
  exports: [MaterialComponents]
})
export class MaterialModule { }
