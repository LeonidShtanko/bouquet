import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { CreateOwnComponent } from './components/create-own/create-own.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { ClientPageComponent } from './components/client-page/client-page.component';
import { AdminPageComponent } from './components/admin-page/admin-page.component';
import { ModalComponent } from './components/modals/modal/modal.component';
import { RefDirective } from './ref.directive';
import { AppRoutingModule } from './app-routing.module';
import { FlowerFilterPipe } from './pipes/flower-filter.pipe';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { FlowerComponent } from './components/flower/flower.component';
import { BouquetComponent } from './components/bouquet/bouquet.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CreateOwnComponent,
    ClientPageComponent,
    AdminPageComponent,
    ModalComponent,
    RefDirective,
    FlowerFilterPipe,
    SignUpComponent,
    FlowerComponent,
    BouquetComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [MomentDateAdapter],
  entryComponents: [ModalComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
