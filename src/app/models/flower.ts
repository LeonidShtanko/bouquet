export interface Flowers {
    img: string
    name: string
    price: number
    count: number
    isChecked: boolean
  }