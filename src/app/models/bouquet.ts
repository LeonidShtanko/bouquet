export interface Bouquets {
    img: string
    name: string
    price: number
    isChecked: boolean
    isDisabled: boolean
  }
  