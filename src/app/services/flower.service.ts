import { Injectable } from '@angular/core';
import { Flowers } from 'src/app/models/flower';

@Injectable({
  providedIn: 'root'
})

export class FlowerService {

  // =======================From backend========================
  flowerCardsArray: Flowers[] = [
    {
      img: '../../assets/images/rosa.jpg',
      name: 'Red roses',
      price: 5,
      count: 0,
      isChecked: false
    },
    {
      img: '../../assets/images/lilies.jpg',
      name: 'White lilies',
      price: 10,
      count: 0,
      isChecked: false
    },
    {
      img: '../../assets/images/pink_rose.jpg',
      name: 'Pink roses',
      price: 15,
      count: 0,
      isChecked: false
    },
    {
      img: '../../assets/images/rosa.jpg',
      name: 'Red roses1',
      price: 5,
      count: 0,
      isChecked: false
    },
    {
      img: '../../assets/images/lilies.jpg',
      name: 'White lilies1',
      price: 10,
      count: 0,
      isChecked: false
    },
    {
      img: '../../assets/images/pink_rose.jpg',
      name: 'Pink roses1',
      price: 15,
      count: 0,
      isChecked: false
    },
  ];
  // =============================================================
  // =======================NEW FLOWER ARRAY (need to make flower cards imposible to change back-end array)=============================
  newFlowArr = this.flowerCardsArray.slice();

  flowerDeliveryArray: Flowers[] = [];
}
