import { Injectable } from '@angular/core';
import { Bouquets } from '../models/bouquet';

@Injectable({
  providedIn: 'root'
})
export class BouquetService {


   // =======================From back-end========================
   bouquetCardsArray: Bouquets[] = [
    {
      img: '../../assets/images/DIY.jpg',
      name: 'DIY',
      price: 0,
      isChecked: false,
      isDisabled: false
    },
    {
      img: '../../assets/images/presentation.jpg',
      name: 'Presentation',
      price: 15,
      isChecked: false,
      isDisabled: false
    },
    {
      img: '../../assets/images/classic.jpg',
      name: 'Classic',
      price: 20,
      isChecked: false,
      isDisabled: false
    },
  ];
  // =======================NEW BOUQUET ARRAY (need to make flower cards imposible to change back-end array)=============================
  newBouqArr = this.bouquetCardsArray.slice();

  bouquetDeliveryArray: Bouquets[] = [];
}
